package de.unirostock.data;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.unirostock.data.PersonWrapper;

public class PublicationWrapper {

	
	private String title;
	private String jounral;
	private String affiliation;
	private String synopsis;
	private String year;
	private List<PersonWrapper> authors = new LinkedList<PersonWrapper>();
	
	public void addAuthor(PersonWrapper author){
		authors.add(author);
	}
	
	public PublicationWrapper(String title, String jounral, String synopsis, String affiliation, String year, List<PersonWrapper> authors){
		this.affiliation = affiliation;
		this.jounral = jounral;
		this.synopsis = synopsis;
		this.title = title;
		this.year = year;			
		if (authors!=null) this.authors = authors;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String titel) {
		this.title = titel;
	}

	public String getJounral() {
		return jounral;
	}

	public void setJounral(String jounral) {
		this.jounral = jounral;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public List<PersonWrapper> getAuthors() {
		return authors;
	}

	public void setAuthors(List<PersonWrapper> authors) {
		this.authors = authors;
	}

	//call to ensure that all properties are set, avoiding error when storing in neo4j
	public void repairNullStrings(){
		if (StringUtils.isEmpty(affiliation)) affiliation="";
		if (StringUtils.isEmpty(jounral)) jounral = "";
		if (StringUtils.isEmpty(synopsis)) synopsis="";
		if (StringUtils.isEmpty(title)) title="";
		if (StringUtils.isEmpty(year)) year="";;			

	}
}
