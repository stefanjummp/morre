package de.unirostock.analyzer;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

import de.unirostock.configuration.Property;

public class PersonExactIndexAnalyzer extends Analyzer{
	

	protected final static PerFieldAnalyzerWrapper personExactIndexAnalyzer =  createpersonExactAnalyzer();
	private final static PerFieldAnalyzerWrapper createpersonExactAnalyzer() {		
		Map<String, Analyzer> map = new HashMap<String, Analyzer>();
		map.put(Property.Person.FAMILYNAME, new LowerCaseKeywordAnalyzer());
		map.put(Property.Person.GIVENNAME, new LowerCaseKeywordAnalyzer());
		map.put(Property.Person.ORGANIZATION, new LowerCaseKeywordAnalyzer());
		return new PerFieldAnalyzerWrapper(new StandardAnalyzer(Version.LUCENE_31), map);
	}
	


		@Override
		public TokenStream tokenStream(String fieldName, Reader reader) {
			return personExactIndexAnalyzer.tokenStream(fieldName, reader);
		}
		

		public static PerFieldAnalyzerWrapper getPersonExactIndexAnalyzer() {
			return personExactIndexAnalyzer;
		}



}
