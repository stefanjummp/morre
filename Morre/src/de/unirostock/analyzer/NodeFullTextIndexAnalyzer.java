package de.unirostock.analyzer;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

import de.unirostock.configuration.Property;

public class NodeFullTextIndexAnalyzer extends Analyzer{
	

	protected final static PerFieldAnalyzerWrapper nodeFullTextIndexAnalyzer =  createNodeFullTextIndexAnalyzer();
	private final static PerFieldAnalyzerWrapper createNodeFullTextIndexAnalyzer() {
		Map<String, Analyzer> map = new HashMap<String, Analyzer>();
		map.put(Property.General.URI, new KeywordAnalyzer());		
		return new PerFieldAnalyzerWrapper(new StandardAnalyzer(Version.LUCENE_31), map);
	}
	
	@Override
	public TokenStream tokenStream(String fieldName, Reader reader) {
		return nodeFullTextIndexAnalyzer.tokenStream(fieldName, reader);
	}

	public static PerFieldAnalyzerWrapper getNodeFullTextIndexAnalyzer() {
		return nodeFullTextIndexAnalyzer;
	}

}
