package de.unirostock.query.enumerator;


public enum IndexEnumerator {

	MODELINDEX,
	ANNOTATIONINDEX,
	PUBLICATIONINDEX,
	PERSONINDEX;
	
	public String getElementNameEquivalent() {
		switch (this) {
		case MODELINDEX:
			return "MODELINDEX";
		case ANNOTATIONINDEX:
			return "ANNOTATIONINDEX";
		case PUBLICATIONINDEX:
			return "PUBLICATIONINDEX";
		case PERSONINDEX:
			return "PERSONINDEX";	
		default:
			return "unknown";
		}
	} 
}
