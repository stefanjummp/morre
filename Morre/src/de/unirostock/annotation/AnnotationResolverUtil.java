package de.unirostock.annotation;

import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;

import de.unirostock.configuration.Property;
import de.unirostock.database.Manager;

public class AnnotationResolverUtil {
	
	private static AnnotationResolverUtil INSTANCE = null;
	
	private GraphDatabaseService graphDB = null;
	private AtomicLong urlCount= null;
	private ExecutorService urlThreadPool = Executors.newFixedThreadPool(10);
	private ExecutorService uriThreadPool = Executors.newFixedThreadPool(5);
	
	public static synchronized AnnotationResolverUtil instance() {
		if (INSTANCE == null) {
			INSTANCE = new AnnotationResolverUtil();
		}
		return INSTANCE;
	}
	
	private AnnotationResolverUtil(){
		graphDB = Manager.instance().getDatabase();
		urlCount= new AtomicLong(0);
	}
 


	
	public void fillAnnotationFullTextIndex(){

		System.out.println("Creating URI list from DB...");
	
		long uriCounter = 0;	
		String uri = "";
		
		for (Iterator<Node> nodeIt = graphDB.getAllNodes().iterator(); nodeIt.hasNext();) {
			Node node = (Node) nodeIt.next();
			if (node.hasProperty(Property.General.NODE_TYPE)){
				if (StringUtils.equals((String)node.getProperty(Property.General.NODE_TYPE),Property.NodeType.RESOURCE)){
					uri = (String)node.getProperty(Property.General.URI);
					ResolveThread rt = new ResolveThread(uri, ++uriCounter);
					rt.setName(uri);
					uriThreadPool.execute(rt);						
				}
			}
		}
		
		System.out.println("Started to resolve " + uriCounter + "URIs...");		
	
		uriThreadPool.shutdown();
		// Wait until all threads are finish
		while (!uriThreadPool.isTerminated()) {

		}
		//TODO implement logging!
		//TODO really, implement logging for this!!!
		/*
		try {
			BufferedWriter w = new BufferedWriter(new FileWriter(System.getProperty("user.dir")+"/miriamErrors.txt"));
			w.append(errorBuilder.toString());
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		System.out.println("done!");		
		
		System.out.println("Started to retrieve " + urlCount + "URLs...");
				
//		for (Iterator<String> uriUrlMapIt = uriUrlMapping.keySet().iterator(); uriUrlMapIt.hasNext();) {
//			uri = (String) uriUrlMapIt.next();
//			List<String> urlList = uriUrlMapping.get(uri);
//			for (Iterator<String> urlListIt = urlList.iterator(); urlListIt.hasNext();) {
//				String url = (String) urlListIt.next();
//				FetchThread ft = new FetchThread(uri, url, ++urlCounter);
//				ft.setName(url);
//				urlThreadPool.execute(ft);
//			}
//		}
//		System.out.println("Threadpool for URLs created, waiting...");
		urlThreadPool.shutdown();
		// Wait until all threads are finish
		while (!urlThreadPool.isTerminated()) {
	
		}
		System.out.println("done!");
		
		System.out.println("Annotation Index created.");
	}
	
	
	public synchronized void  addToUrlThreadPool(String uri, String url){		
		FetchThread ft = new FetchThread(uri, url, urlCount.getAndIncrement());
		ft.setName(url);
		urlThreadPool.execute(ft);
	}


}
