package de.unirostock.query.enumerator;

import de.unirostock.configuration.Property;

public enum ModelFieldEnumerator {

	// model index:
	NAME, REACTION, SPECIES, COMPARTMENT, NON_RDF, CREATOR, AUTHOR, URI,

	//not specified
	NONE;
	
	
	public String getElementNameEquivalent() {
		switch (this) {
		case NAME:
			return Property.SBML.NAME;
		case REACTION:
			return Property.SBML.REACTION;
		case SPECIES:
			return Property.SBML.SPECIES;
		case COMPARTMENT:
			return Property.SBML.COMPARTMENT;
		case NON_RDF:
			return Property.SBML.NON_RDF;
		case AUTHOR:
			return Property.Publication.AUTHOR;
		case CREATOR:
			return Property.General.CREATOR;
		case URI:
			return Property.General.URI;
		case NONE:
			return "NONE";	
		default:
			return "NONE";
		}
	}
	
	public int getElementWeightEquivalent() {
		switch (this) {
		case NAME:
			return 3;
		case REACTION:
			return 2;
		case SPECIES:
			return 2;
		case COMPARTMENT:
			return 2;
		case NON_RDF:
			return 1;
		case AUTHOR:
			return 2;
		case CREATOR:
			return 1;
		case URI:
			return 2;
		case NONE:
			return 1;	
		default:
			return 1;
		}
	}
}
