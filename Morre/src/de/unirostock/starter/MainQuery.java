package de.unirostock.starter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.unirostock.configuration.Config;
import de.unirostock.database.Manager;
import de.unirostock.query.QueryAdapter;
import de.unirostock.query.ResultSet;
import de.unirostock.query.enumerator.AnnotationFieldEnumerator;
import de.unirostock.query.enumerator.ModelFieldEnumerator;
import de.unirostock.query.enumerator.PersonFieldEnumerator;
import de.unirostock.query.enumerator.PublicationFieldEnumerator;
import de.unirostock.query.types.AnnotationQuery;
import de.unirostock.query.types.InterfaceQuery;
import de.unirostock.query.types.ModelQuery;
import de.unirostock.query.types.PersonQuery;
import de.unirostock.query.types.PublicationQuery;
import de.unirostock.util.ResultSetUtil;

public class MainQuery {

	public static void main(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-dbPath")) {
				Config.dbPath = args[++i];
			}
		}

		// create neo4j database
		long start = System.currentTimeMillis();
		System.out.println("Started at: " + new Date());
		System.out.print("Getting manager...");
		Manager.instance();
		System.out.println("done in " + (System.currentTimeMillis() - start)
				+ "ms");
		System.out.println();

		String s = "";
		while (!s.equals("exit")) {
			System.out
					.println("QueryType: (1) simple, (2) anno, (3) modeliq, (4) annoiq, (5) persiq, (6) pubiq (7) all : ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}

			switch (Integer.valueOf(s)) {
				case 1:
					simple(); break;
				case 2: 
					anno(); break;
				case 3:
					modelInterfaceQuery(); break;
				case 4: 
					annoInterfaceQuery(); break;
				case 5:
					personInterfaceQuery(); break;
				case 6:
					publicationInterfaceQuery(); break;
				case 7:
					allInterfaceQuery(); break;	
			default:
				continue;
			}
		}
	}
	
	private static void simple() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from simple index...");
			List<ResultSet> results = null;
			try {
				results = QueryAdapter.simpleQuery(s);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}

	}

	private static void anno() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from anno index...");
			List<ResultSet> results = null;
			try {
				results = QueryAdapter.annotationQuery(s, 10);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}	
	}
	
	private static void annoInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from anno interface...");
			List<ResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCE, s);
				List<InterfaceQuery> aqL = new LinkedList<InterfaceQuery>();
				aqL.add(aq);
				results = QueryAdapter.executeInterfaceQueries(aqL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}

	}
	
	private static void personInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from person interface...");
			List<ResultSet> results = null;
			try {
				PersonQuery pq = new PersonQuery();
				pq.addQueryClause(PersonFieldEnumerator.NONE, s);
				List<InterfaceQuery> qL = new LinkedList<InterfaceQuery>();
				qL.add(pq);
				results = QueryAdapter.executeInterfaceQueries(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}	
	}
	
	
	private static void modelInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from model interface...");
			List<ResultSet> results = null;
			try {
				ModelQuery pq = new ModelQuery();
				pq.addQueryClause(ModelFieldEnumerator.NONE, s);
				List<InterfaceQuery> qL = new LinkedList<InterfaceQuery>();
				qL.add(pq);
				results = QueryAdapter.executeInterfaceQueries(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}

	}
	
	
	private static void publicationInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from publication interface...");
			List<ResultSet> results = null;
			try {
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				List<InterfaceQuery> qL = new LinkedList<InterfaceQuery>();
				qL.add(pq);
				results = QueryAdapter.executeInterfaceQueries(qL);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
			System.out.println("done");
		}

	}
	
	
	private static void allInterfaceQuery() {
		String s = "";
		while (!s.equals("exit")) {
			System.out.println("Query: ");
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						System.in));
				s = in.readLine();

			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
			if (s.equals("exit")) {
				break;
			}
			System.out.println("Retrieving from all interfaces...");
			List<ResultSet> results = null;
			try {
				AnnotationQuery aq = new AnnotationQuery();
				aq.setBestN(20);
				//aq.setThreshold(0.01f);
				aq.addQueryClause(AnnotationFieldEnumerator.RESOURCE, s);
				
				PersonQuery ppq = new PersonQuery();
				ppq.addQueryClause(PersonFieldEnumerator.NONE, s);
				
				ModelQuery mq = new ModelQuery();
				mq.addQueryClause(ModelFieldEnumerator.NONE, s);
				
				PublicationQuery pq = new PublicationQuery();
				pq.addQueryClause(PublicationFieldEnumerator.NONE, s);
				List<InterfaceQuery> qL = new LinkedList<InterfaceQuery>();
				
				qL.add(pq);
				qL.add(aq);
				qL.add(ppq);
				qL.add(mq);
				
				results = QueryAdapter.executeInterfaceQueries(qL);
				results = ResultSetUtil.collateByModelId(results);
				results = ResultSetUtil.sortByScore(results);
			} catch (Exception e) {
				e.printStackTrace();
			}
			printResults(results);
		}
			System.out.println("done");


	}
	
	private static void printResults(List<ResultSet> results){
		if ((results != null) && (results.size() > 0)) {

			System.out.println("Found " + results.size() + " results");
			for (Iterator<ResultSet> iterator = results.iterator(); iterator
					.hasNext();) {
				ResultSet resultSet = (ResultSet) iterator.next();
				System.out
						.println("===============================================");
				System.out.println(resultSet.getModelScore());
				System.out.println(resultSet.getModelName());
				System.out.println(resultSet.getModelId());
				System.out.println(resultSet.getDatabaseId());
				System.out.println(resultSet.getFilename());
				System.out.println(resultSet.getFilepath());
			}

		} else
			System.out.print("No results!");
		System.out.println();
		System.out
				.println("||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println();
	}
	
}
