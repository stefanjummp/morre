package de.unirostock.query;


import org.apache.commons.lang.StringUtils;

public class ResultSet {
	private String modelName;
	private float score;
	private String modelID;
	private String explanation;
	private long databaseID;
	private String filepath;
	private String filename;
	
	public ResultSet(float score, String modelId, String modelName, Long databaseID, String filepath, String filename, String explanation){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.explanation = explanation;
		this.databaseID = databaseID;
		this.filepath = filepath;
		this.filename = filename;
	}
	
	public ResultSet(float score, String modelId, String modelName, Long databaseID, String explanation){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.explanation = explanation;
		this.databaseID = databaseID;
		
	}
	
	public ResultSet(float score, String modelId, String modelName){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
	}
	
	public ResultSet(float score, String modelId, String modelName, String filepath, String filename){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.filepath = filepath;
		this.filename = filename;
	}	
	
	public String getModelName() {
		return modelName;
	}
	
	public String getModelId() {
		return modelID;
	}

	public float getModelScore() {
		return score;
	}

	public String getSearchExplanation() {
		return explanation;
	}

	public Long getDatabaseId() {
		return databaseID;
	}
	
	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public boolean equals(Object resultSet) {
		ResultSet rs; 
		if ((resultSet==null) || !(resultSet instanceof ResultSet)) return false;
		else rs = (ResultSet) resultSet; 
		
		if (!StringUtils.equals(this.modelID, rs.getModelId())) return false;
		
		if (!StringUtils.equals(this.filepath, rs.getFilepath())) return false;
		
		if (!StringUtils.equals(this.filename, rs.getFilename())) return false;
		
		if (!StringUtils.equals(this.modelName, rs.getModelName())) return false;
		
		if (!StringUtils.equals(this.explanation, rs.getSearchExplanation())) return false;
		
		if (this.score != rs.score) return false;
		
		if (this.databaseID != rs.databaseID) return false;
		
		return true;
	}


}
