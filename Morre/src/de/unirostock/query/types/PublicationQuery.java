package de.unirostock.query.types;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;

import de.unirostock.analyzer.PublicationFullTextIndexAnalyzer;
import de.unirostock.configuration.Property;
import de.unirostock.database.DBTraverser;
import de.unirostock.database.Manager;
import de.unirostock.query.ResultSet;
import de.unirostock.query.enumerator.PublicationFieldEnumerator;

public class PublicationQuery implements InterfaceQuery {

	private final Analyzer analyzer = PublicationFullTextIndexAnalyzer.getPublicationFullTextIndexAnalyzer();
	private final Index<Node> index = Manager.instance().getPublicationFullTextIndex();
	private final String[] indexedFields = {Property.Publication.ABSTRACT,
											Property.Publication.AFFILIATION, Property.Publication.AUTHOR, 
											Property.Publication.JOURNAL, Property.Publication.TITLE, 
											Property.Publication.YEAR};
	private Map<PublicationFieldEnumerator, List<String>> queryMap =  new HashMap<PublicationFieldEnumerator, List<String>>();
	
	@Override
	public Analyzer getAnalyzer() {
		return analyzer;
	}

	@Override
	public Index<Node> getIndex() {
		return index;
	}

	@Override
	public String[] getIndexedFields() {
		return indexedFields;
	}

	@Override
	public Query getQuery() {
		Query q = null;
		try {
			q = createQueryFromQueryMap();
		} catch (ParseException e) {
			//TODO log me
			q = null;
		}
		return q;
	}

	@Override
	public List<ResultSet> getResults() {
		return retrieveResults();
	}

	@Override
	public PublicationFieldEnumerator[] getIndexFields() {
		return PublicationFieldEnumerator.class.getEnumConstants();
	}

	public void addQueryClause(PublicationFieldEnumerator field, String queryString) {
		
		if (PublicationFieldEnumerator.NONE.equals(field)){
			//if a NONE field was provided skip the list and expand to all
			queryMap = new HashMap<PublicationFieldEnumerator, List<String>>();
			PublicationFieldEnumerator[] pe = getIndexFields();
			for (int i = 0; i < pe.length; i++) {
				PublicationFieldEnumerator e = pe[i];
				if (e.equals(PublicationFieldEnumerator.NONE)) continue;
				List<String> termList = new LinkedList<String>();
				termList.add(queryString);
				queryMap.put(e, termList);
			}
		} else {
			//add single field -> string pair
			List<String> termList = null; 
			if (queryMap.keySet().contains(field)){
				termList = queryMap.get(field);
			} else {
				termList = new LinkedList<String>();
			}
			termList.add(queryString);
			
			queryMap.put(field, termList);
		}
	}
	
	private Query createQueryFromQueryMap() throws ParseException{
		if (queryMap.isEmpty()) return null;
		
		StringBuffer q = new StringBuffer();
		for (Iterator<PublicationFieldEnumerator> queryMapIt = queryMap.keySet().iterator(); queryMapIt.hasNext();) {
			PublicationFieldEnumerator pe = (PublicationFieldEnumerator) queryMapIt.next();
			List<String> termList = queryMap.get(pe);
			for (Iterator<String> termListIt = termList.iterator(); termListIt.hasNext();) {
				String term = (String) termListIt.next();
				if (StringUtils.isEmpty(term)) continue;
				q.append(pe.getElementNameEquivalent());
				q.append(":(");
				q.append(term);
				q.append(")^");
				q.append(pe.getElementWeightEquivalent());
				q.append(" ");

			}
		}		
		QueryParser qp = new QueryParser(Version.LUCENE_31, Property.Publication.TITLE, analyzer);		 
		return qp.parse(q.toString());		
	}
	
	private List<ResultSet> retrieveResults(){
		if (queryMap.isEmpty()) return new LinkedList<ResultSet>();
		
		Query q = null;
		try {
			q = createQueryFromQueryMap();
		} catch (ParseException e) {
			// TODO log me
			return new LinkedList<ResultSet>();
		}
		
		IndexHits<Node> hits = index.query(q);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>();

		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {		
			Node node = (Node) hitsIt.next();
			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.PUBLICATION)){
				result.addAll(DBTraverser.getResultSetFromNode(node, hits.currentScore()));				
			}
		}
		return result;
	}

}
