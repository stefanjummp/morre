package de.unirostock.database;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.Traverser;

import de.unirostock.configuration.Property;
import de.unirostock.configuration.Relation.RelTypes;
import de.unirostock.query.ResultSet;
import de.unirostock.util.NodeTypReturnEvaluater;

public class DBTraverser {

//	private StopEvaluator stopEvaluatorModel = new StopEvaluator() {
//		@Override
//		public boolean isStopNode(TraversalPosition arg0) {
//			Node node = arg0.currentNode();
//			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.MODEL)){	
//			  return true;
//			} else return false;
//		}
//	};
	
	
//	public static Node fromModelToAnnotation(Node modelNode) {
//		Node annoNode = null;
//		if ((modelNode!=null) && modelNode.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals(Property.NodeType.MODEL, (String)modelNode.getProperty(Property.General.NODE_TYPE))){
//			annoNode = modelNode.getSingleRelationship(RelTypes.HAS_ANNOTATION, Direction.OUTGOING).getEndNode();
//		} 
//		if ((annoNode!=null) && annoNode.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals(Property.NodeType.ANNOTATION, (String)annoNode.getProperty(Property.General.NODE_TYPE))){
//			return annoNode;
//		}
//		return null;
//	}
	
	public static Node fromModelToDocument(Node modelNode) {
		if (modelNode==null) return null;
		Node docNode = null;
		if ((modelNode!=null) && modelNode.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals(Property.NodeType.MODEL,(String) modelNode.getProperty(Property.General.NODE_TYPE))){
			docNode = modelNode.getSingleRelationship(RelTypes.BELONGS_TO, Direction.OUTGOING).getEndNode();
		} 
		if ((docNode!=null) && docNode.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals(Property.NodeType.DOCUMENT, (String) docNode.getProperty(Property.General.NODE_TYPE))){
			return docNode;
		}
		return null;
		
	}

	public static Node fromNodeToAnnotation(Node node){
		if (node==null) return null;
		Relationship rs = node.getSingleRelationship(RelTypes.HAS_ANNOTATION, Direction.OUTGOING);
		
		if (rs==null) return null;
		Node annoNode = rs.getEndNode();
		
		if ((annoNode!=null) && annoNode.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals(Property.NodeType.ANNOTATION, (String) annoNode.getProperty(Property.General.NODE_TYPE))){
			return annoNode;
		}
		return null;
	}
	
	public static List<Node> getModelsFromNode(Node node){
		LinkedList<Node> modelList = new LinkedList<Node>();
		if (node==null) return modelList;
		Traverser t = node.traverse(Traverser.Order.DEPTH_FIRST, StopEvaluator.END_OF_GRAPH, new NodeTypReturnEvaluater(Property.NodeType.MODEL), RelTypes.BELONGS_TO, Direction.OUTGOING);
		for (Iterator<Node> it = t.iterator(); it.hasNext();) {
			Node tNode = (Node) it.next();
			modelList.add(tNode);
		}		
		return modelList;
	}
	
	
	public static List<ResultSet> getResultSetFromNode(Node node, float score) {
		List<ResultSet> result = new LinkedList<ResultSet>();
		List<Node> modelList = getModelsFromNode(node);
		for (Iterator<Node> iterator = modelList.iterator(); iterator.hasNext();) {
			Node modelNode = (Node) iterator.next();
			Node docNode = DBTraverser.fromModelToDocument(modelNode);
			Long databaseId = null;
			if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
			ResultSet rs = new ResultSet(score, (String)modelNode.getProperty(Property.General.ID), (String)modelNode.getProperty(Property.SBML.NAME), databaseId, null);
			if (docNode.hasProperty("filepath")) rs.setFilepath((String)docNode.getProperty("filepath"));
			if (docNode.hasProperty("filename")) rs.setFilename((String)docNode.getProperty("filename"));
			result.add(rs);
			}
		return result;
	}
	
//	public static List<ResultSet> getResultsFromPersonToModel(Node personNode, float score) {
//		List<ResultSet> result = new LinkedList<ResultSet>();
//		Traverser t = personNode.traverse(Traverser.Order.BREADTH_FIRST, StopEvaluator.END_OF_GRAPH, ReturnableEvaluator.ALL_BUT_START_NODE, RelTypes.BELONGS_TO, Direction.OUTGOING);
//		for (Iterator<Node> iterator = t.iterator(); iterator.hasNext();) {
//			Node node = (Node) iterator.next();
//			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.MODEL)){
//				Node docNode = DBTraverser.fromModelToDocument(node);
//				Long databaseId = null;
//				if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
//				
//				ResultSet rs = new ResultSet(score, (String)node.getProperty(Property.General.ID), (String)node.getProperty(Property.SBML.NAME), databaseId, null);
//				result.add(rs);
//			}
//		}
//		return result;
//	}
//	
//	public static List<ResultSet> getResultsFromAnnotationToModel(Node annoNode, float score) {
//		List<ResultSet> result = new LinkedList<ResultSet>();
//		Traverser t = annoNode.traverse(Traverser.Order.BREADTH_FIRST, StopEvaluator.END_OF_GRAPH, ReturnableEvaluator.ALL_BUT_START_NODE, RelTypes.BELONGS_TO, Direction.OUTGOING);
//		for (Iterator<Node> iterator = t.iterator(); iterator.hasNext();) {
//			Node node = (Node) iterator.next();
//			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.MODEL)){
//				Node docNode = DBTraverser.fromModelToDocument(node);
//				Long databaseId = null;
//				if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
//				
//				ResultSet rs = new ResultSet(score, (String)node.getProperty(Property.General.ID), (String)node.getProperty(Property.SBML.NAME), databaseId, null);
//				result.add(rs);
//			}
//		}
//		return result;
//	}

}
