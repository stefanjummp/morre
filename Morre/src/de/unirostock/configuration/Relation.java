package de.unirostock.configuration;

import org.neo4j.graphdb.RelationshipType;

public class Relation {

	public static enum RelTypes implements RelationshipType
	{
		/*
		 * Relations to describe XML Parent-Child relations
		 */
	    CHILD,
	    PARENT,
	    HAS_ANNOTATION,
	    
	    /*
	     * Relations to describe documents
	     */
	    HAS_DOCUMENT,
	    HAS_MODEL,
	    BELONGS_TO,
	    HAS_COMPARTMENT, 
	    HAS_SPECIES, 
	    HAS_PARAMETER,
	    IS_LOCATED_IN, 
	    CONTAINS_SPECIES,
	    CONTAINS_REACTION,
	    IS_MODIFIER, 
	    HAS_REACTION, 
	    HAS_MODIFIER, 
	    HAS_PRODUCT, 
	    IS_PRODUCT, 
	    HAS_REACTANT, 
	    IS_REACTANT, 
	    IS_CREATOR, 
	    HAS_RULE, 
	    HAS_FUNCTION, 
	    HAS_EVENT, 
	    HAS_PUBLICATION, 
	    HAS_SBOTERM
	}
	
	
}
