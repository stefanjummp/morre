package de.unirostock.query.enumerator;

import de.unirostock.configuration.Property;

public enum AnnotationFieldEnumerator {

	// annotation index
	URI, RESOURCE,

	//not specified
	NONE;
	
	
	public String getElementNameEquivalent() {
		switch (this) {
		case URI:
			return Property.General.URI;
		case RESOURCE:
			return Property.General.RESOURCE;
		case NONE:
			return "NONE";	
		default:
			return "NONE";
		}
	}
	
	public int getElementWeightEquivalent(){
		switch (this) {
		case URI:
			return 4;
		case RESOURCE:
			return 2;
		case NONE:
			return 1;	
		default:
			return 1;
	}
}

}
