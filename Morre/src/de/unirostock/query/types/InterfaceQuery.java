package de.unirostock.query.types;

import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;

import de.unirostock.query.ResultSet;

public interface InterfaceQuery {
	
	public Analyzer getAnalyzer();
	
	public Index<Node> getIndex();
	
	public String[] getIndexedFields();
	
	public Enum<?>[] getIndexFields();
	
	public Query getQuery();
	
	public List<ResultSet> getResults();
	
	//public void addQueryClause(String field, String query);
	
	//public void addQueryClause(Enum<?> field, String query);
	

}
