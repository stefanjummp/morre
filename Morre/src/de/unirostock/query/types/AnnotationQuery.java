package de.unirostock.query.types;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;

import de.unirostock.analyzer.AnnotationIndexAnalyzer;
import de.unirostock.configuration.Property;
import de.unirostock.database.DBTraverser;
import de.unirostock.database.Manager;
import de.unirostock.query.ResultSet;
import de.unirostock.query.enumerator.AnnotationFieldEnumerator;

public class AnnotationQuery implements InterfaceQuery {
	private final Analyzer analyzer = AnnotationIndexAnalyzer.getAnnotationIndexAnalyzer();
	private final Index<Node> index = Manager.instance().getAnnotationFullTextIndex();
	private final String[] indexedFields = {Property.General.URI, Property.General.RESOURCE};
	private Map<AnnotationFieldEnumerator, List<String>> queryMap =  new HashMap<AnnotationFieldEnumerator, List<String>>();
	
	
	private float threshold = -1f;
	private int bestN = Integer.MAX_VALUE;


	public float getThreshold() {
		return threshold;
	}

	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

	public int getBestN() {
		return bestN;
	}

	public void setBestN(int bestN) {
		this.bestN = bestN;
	}

	
	@Override
	public Analyzer getAnalyzer() {
		return analyzer;
	}

	@Override
	public Index<Node> getIndex() {
		return index;
	}

	@Override
	public String[] getIndexedFields() {
		return indexedFields;
	}

	@Override
	public Query getQuery() {
		Query q = null;
		try {
			q = createQueryFromQueryMap();
		} catch (ParseException e) {
			//TODO log me
			q = null;
		}
		return q;
	}

	@Override
	public List<ResultSet> getResults() {
		return retrieveResults();
	}

	@Override
	public AnnotationFieldEnumerator[] getIndexFields() {
		return AnnotationFieldEnumerator.class.getEnumConstants();
	}

	public void addQueryClause(AnnotationFieldEnumerator field, String queryString) {
		
		if (AnnotationFieldEnumerator.NONE.equals(field)){
			//if a NONE field was provided skip the list and expand to all
			queryMap = new HashMap<AnnotationFieldEnumerator, List<String>>();
			AnnotationFieldEnumerator[] pe = getIndexFields();
			for (int i = 0; i < pe.length; i++) {
				AnnotationFieldEnumerator e = pe[i];
				if (e.equals(AnnotationFieldEnumerator.NONE)) continue;
				List<String> termList = new LinkedList<String>();
				termList.add(queryString);
				queryMap.put(e, termList);
			}
		} else {
			//add single field -> string pair
			List<String> termList = null; 
			if (queryMap.keySet().contains(field)){
				termList = queryMap.get(field);
			} else {
				termList = new LinkedList<String>();
			}
			termList.add(queryString);
			
			queryMap.put(field, termList);
		}
	}
	
	private Query createQueryFromQueryMap() throws ParseException{
		if (queryMap.isEmpty()) return null;
		
		StringBuffer q = new StringBuffer();
		for (Iterator<AnnotationFieldEnumerator> queryMapIt = queryMap.keySet().iterator(); queryMapIt.hasNext();) {
			AnnotationFieldEnumerator pe = (AnnotationFieldEnumerator) queryMapIt.next();
			List<String> termList = queryMap.get(pe);
			for (Iterator<String> termListIt = termList.iterator(); termListIt.hasNext();) {
				String term = (String) termListIt.next();
				if (StringUtils.isEmpty(term)) continue;
				q.append(pe.getElementNameEquivalent());
				q.append(":(");
				q.append(term);
				q.append(")^");
				q.append(pe.getElementWeightEquivalent());
				q.append(" ");
			}
		}		
		QueryParser qp = new QueryParser(Version.LUCENE_31, Property.General.RESOURCE, analyzer);		 
		return qp.parse(q.toString());		
	}
	
	private List<ResultSet> retrieveResults(){
		if (queryMap.isEmpty()) return new LinkedList<ResultSet>();
		
		Query q = null;
		try {
			q = createQueryFromQueryMap();
		} catch (ParseException e) {
			// TODO log me
			return new LinkedList<ResultSet>();
		}
		
		IndexHits<Node> hits = index.query(q);

		if ((hits == null) || (hits.size() == 0)) {
			return new LinkedList<ResultSet>();
		}
		List<ResultSet> result = new LinkedList<ResultSet>();
		
		int resultCount = 1;
		for (Iterator<Node> hitsIt = hits.iterator(); hitsIt.hasNext();) {
			Node node = (Node) hitsIt.next();
			float score = hits.currentScore();
			//end loop if bestN or threshold are met
			if ((resultCount >= bestN) || (score < threshold)) break;
			if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), Property.NodeType.RESOURCE)){
				result.addAll(DBTraverser.getResultSetFromNode(node, score));				
			}
			resultCount++;
		}
		return result;
	}

}
